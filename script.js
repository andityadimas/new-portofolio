document.querySelector("form").addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent form submission

    var formData = new FormData(this);

    fetch(this.action, {
        method: this.method,
        body: formData
    })
        .then(function (response) {
            if (response.ok) {
                // Handle successful email submission
                console.log("Email sent successfully");
            } else {
                // Handle error in email submission
                console.error("Failed to send email");
            }
        })
        .catch(function (error) {
            console.error("An error occurred:", error);
        });
});
